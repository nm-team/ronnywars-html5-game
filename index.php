<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body>

<div id="loadFrame">
    <div id="divLoad">
    </div>
    <div id="divLoadBar">
    </div>
    <div id="divLoadNum">
        0%
    </div>
        </div>
<div id="UI">
    <div id="scoreb">Score B : 0</div>
    <div id="player">
        <div id="composent"></div>
        <div id="flag"></div>
    </div>
    <div id="time">Time: 00:00</div>
    <div id="ping">
        <div id="composent"></div>
        <div id="flag"></div>
    </div>
    <div id="scorer">Score R : 0</div>
    <div id="chat">
    </div>
        </div>
<div id="UIBottom">
    <div id="sendChat">
        <input type="text" id="sendMess" maxlength="30" placeholder="Type here...">
    </div>
</div>

<div id="table">
    <div id="B">
        <div id="titre">Team B
        </div>
        <div id="jB">
        </div>
    </div>
    <div id="R">
        <div id="titre">Team R</div>
        <div id="jR"></div>
    </div>
</div>

<div id="bug">
    <a href="https://bitbucket.org/nm-team/ronnywars-html5-game/issues" target="_blank"><img
            src="images/bug.png" style="width=10px;"/> </a>
</div>

<script src="./lib/quintus-v0-2-0-all.js"></script>
<script src='./lib/jquery.js'></script>
<script src="./lib/socket.io.js"></script>
<script>
    var session_id;
    var newsession_id = '<?php session_start(); echo session_id(); session_destroy();?>';
    var capture_flag = false;
    var secondes = 0;
    var minutes = 0;
    var pointB = 0;
    var pointR = 0;
    var w, h;
    var spawn = new Array;
    spawn["B"] = [[10, 76], [600, 600]];//[[xmin,xmax],[ymin,ymax]]
    spawn["R"] = [[2624, 2670], [600, 600]];
    var SPRITE_PLAYER = 0;
    var SPRITE_BULLET = 1;
    var SPRITE_OBJ = 2;
    var SPRITE_PHYS = 3;
    var SPRITE_ZONE = 4;
    var SPRITE_ACTOR = 5;

    var players = [];

    var pingDate = 0;

    var team;
    var socket = io.connect('http://gloird.ovh:88', {
        reconnection: false
    });
    io.managers
    var username = "Guest";

    var selfId;

    var nbTeamB = 0;
    var nbTeamR = 0;

    var my = new Object();

    var Q = new Quintus({
        development: true, // On lance le mode développement, pour forcer le refresh des assets (images, fichiers JSON…). Attention à bien désactiver cela une fois en production !
        audioSupported: ['mp3']
    });
    function readCookie(name) {
        var i, c, ca, nameEQ = name + "=";
        ca = document.cookie.split(';');
        for (i = 0; i < ca.length; i++) {
            c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return '';
    }

    function writeCookie(name, value, days) {
        var date, expires;
        if (days) {
            date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    Q.include([ // On indique quels composants inclure de base
        Quintus.Sprites, // Pour gérer les sprites (les calques en gros)
        Quintus.Scenes, // Pour gérer les scenes (les différentes pages, pour faire simple)
        Quintus.Anim, // Pour gérer les animations (sur des sprites, par exemple)
        Quintus['2D'], // Pour gérer la 2D : permet d'avoir des ennemis qui se déplacent et de détecter les collisions automatiquement
        Quintus.Input, // Pour gérer les contrôles (certains contrôles sont inclus de base, c'est assez pratique)
        Quintus.Touch, // Pour gérer les contrôles via une surcouche tactile (avec un joypad si nécessaire — c'est paramétrable)
        Quintus.UI,// Pour afficher des boutons, du texte, etc.
        Quintus.TMX
    ]);
    Q.include("Audio").enableSound();

    //Q.debug = true;
    //Q.debugFill = true;
    /* Le premier paramètre permet d'indiquer l'id de notre canvas, vous pouvez aussi passer directement un objet du DOM ou ne rien spécifier si vous voulez que la librairie crée un canvas pour vous */
    Q.setup('game', {
        maximize: true, // Inutile de modifier la taille du canvas (`true` permet d'étendre à toute la surface disponible)
        width: 800, // Largeur de base
        height: 1000 // Hauteur de base
    });
    $("#game_container").append("<div id='UI2'></div>");
    socket.on('eventName', function (data) {
        console.log(data);
    });

    Q.controls().touch();
    Q.controls();
</script>
<!--- Chargement js ---->

<script src="./js/Animations.js"></script>
<script src="./js/Component.js"></script>
<script src="./js/Sprite.js"></script>

<script src="./js/function.js"></script>

<script src="./js/scenes/game.js"></script>
<script src="./js/scenes/register.js"></script>
<script src="./js/scenes/startGame.js"></script>
<script src="./js/scenes/StopGame.js"></script>
<script src="./js/scenes/team.js"></script>
<script src="./js/scenes/ready.js"></script>
<script src="./js/scenes/salon.js"></script>

<script src="./js/loadTMX.js"></script>

<!-- Fin chargement ---->

</body>
</html>
<?php
