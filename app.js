var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var mysql      = require('mysql');
var md5 = require("blueimp-md5").md5;
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
    res.render('/index.html');
});

var TIME_MAX = 600;//600
var POINT_MAX = 2;//10
var TIME_BOOTS = 3000;
var TIME_BULLEGUN = 3000;
var TIME_GUN = 10000;


var players=[]; //id,username,team,isReady
var id = 0;
var timeSec = 0;
var pointR = 0;
var pointB = 0;
var alreadyStarted = false;

var salons=[];

var intervalChrono;
var intervalSync;
var intervalBoots;
var intervalBulleGun;
var intervalGun;

io.on('connection', function (socket) {
    var session_id;
    var salon = undefined;
    var connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'ronnystat',
        password : 'QFB8DUaVTjUvtxGK',
        database : 'ronnystat'
    });
    connection.connect();
    id++;
    var thisPlayer = {playerid:id,username:"",team:"",isReady:false};

    socket.on('test', function(data){
        socket.emit(this);
    });
    socket.on('test2', function(data){
        socket.emit('eventName', {data: socket.rooms});
    });

    socket.on('createSalon', function(data){

        salons.push(data);
        salons[salons.length-1]['nb'] = 1;
        salons[salons.length-1]['id'] = salons.length-1;
        console.log(salons);
        salon = salons.length-1;
        console.log(thisPlayer.username+" create the salon :"+salons[salon].name);
        socket.join(salons[salon].name);
        console.log(thisPlayer.username+" join the salon :"+salons[salon].name);
    });

    socket.on('joinSalon', function(data){
        console.log(data);
        var i =0;
        while(salons[i]['name'] != data)
        {
            i++;
        }
        if(salons[i]['name'] == data){
            console.log(i);
            salon = i;
            salons[i]['nb']++;
            socket.join(salons[salon].name);
            console.log(thisPlayer.username+" join the salon :"+salons[salon].name);
            socket.emit('joinSuccess');
        }
    });

    socket.on('quitSalon', function(){
        salons[salon['id']]['nb']--;
        if(salons[salon['id']]['nb'] == 0){
            delete salons[salon];
        }
        salon = null;
        socket.leave(salons[salon].name);
        console.log(thisPlayer.username+" leave the salon :"+salons[salon].name);
    });

    socket.on('refresh',function(){
        socket.emit('refresh',salons);
    });

    socket.on('openSession',function(data){
        session_id = data;
    });

    socket.on('checkSession', function(data){
        var playerInfos;
        connection.query('SELECT * FROM session where id ="'+data+'"', function(err, rows, fields) {

            if (err) throw err;
            if(!rows[0]){
                session_id = data;
                socket.emit('sessionFailed');
            }else{
                connection.query('SELECT U.* FROM session S inner join user U on S.idUser = U.id_user where id = "'+data+'"', function(err, rows, fields) {
                    if (err) throw err;

                    if(rows[0]) {
                        access = true;
                        playerInfos = rows[0];
                        socket.emit('sessionOK',{playerInfo:playerInfos,isLogged:true});
                    }

                });
            }
        });
    });

    socket.on('login', function (data) {
        var access = false;
        var mdp = md5(data.password);
        var playerInfos;
        

        connection.query('SELECT * FROM user where username_user ="'+data.username+'" and password_user = "'+mdp+'"', function(err, rows, fields) {
          if (err) throw err;
          if(!rows[0]){
            access = false;
            socket.emit('isLogged',{playerInfo:playerInfos,isLogged:false});
          }else{
            access = true;
            playerInfos = rows[0];

            socket.emit('isLogged',{playerInfo:playerInfos,isLogged:true});
            connection.query('INSERT INTO `session`( `id`, `idUser`) VALUES ( "'+session_id+'" ,"'+playerInfos.id_user+'")', function(err, rows, fields) {
                if (err) throw err;

            });
          }
        });
    });

    socket.on('register', function (data) {
        var access = false;
        var mdp = md5(data.password);
        var playerInfos;
        connection.query('SELECT * FROM user where username_user ="'+data.username+'" and password_user = "'+mdp+'" and email_user = "'+data.email+'"', function(err, rows, fields) {
          if (err) throw err;
          if(!rows[0]){
            access = true;
            connection.query('INSERT INTO `user`( `username_user`, `password_user`, `email_user`) VALUES ( "'+data.username+'" ,"'+mdp+'","'+data.email+'" )', function(err, rows, fields) {
                if (!err) {
                    socket.emit('isRegister',{isRegister:true});

                }else{
                    socket.emit('isRegister',{isRegister:false});
                }
            });
          }else{
            access = false;
            //delete playerInfos.password_user;
            socket.emit('isRegister',{isRegister:false});
          }
        });

    });

    socket.on('saveStats', function (data) {
        //écrire la requette de sauvegarde en entier, il faut faire les calculs coté client pour eviter le spam de requette
        //console.log(data);
        //console.log('UPDATE user SET nbDeath_user = '+data.nbDeath_user+',nbPlayedGame_user = '+data.nbPlayedGame_user+',nbWinGame_user = '+data.nbWinGame_user+',nbDropFlag_user = '+data.nbDropFlag_user+',nbFlagGet_user = '+data.nbFlagGet_user+',nbBootsGet_user = '+data.nbBootsGet_user+',nbPistolGet_user = '+data.nbPistolGet_user+',nbJump_user = '+data.nbJump_user+',nbShot_user = '+data.nbShot_user+' where id_user ='+data.id_user);
        connection.query('UPDATE user SET nbDeath_user = '+data.nbDeath_user+',nbPlayedGame_user = '+data.nbPlayedGame_user+',nbWinGame_user = '+data.nbWinGame_user+',nbDropFlag_user = '+data.nbDropFlag_user+',nbFlagGet_user = '+data.nbFlagGet_user+',nbBootsGet_user = '+data.nbBootsGet_user+',nbPistolGet_user = '+data.nbPistolGet_user+',nbJump_user = '+data.nbJump_user+',nbShot_user = '+data.nbShot_user+' where id_user ='+data.id_user, function(err, rows, fields) {
          if (err) throw err;
        });
    });

    socket.on('sendUsername', function (data) {
        thisPlayer.username = data.username;
        socket.emit('defineTeam', {teamB:countTeam("B"),teamR:countTeam("R")});
    });

    socket.on('sendTeam', function (data) {
        thisPlayer.team = data.team;
        players.push(thisPlayer);
    });

    socket.on('sendReady', function (data) {
        thisPlayer.isReady = data.ready;
        socket.broadcast.to(salon).emit('readyPlayers',{players:players});
        socket.emit('readyPlayers',{players:players});
        if (data.ready && players.length > 1){
            var i = 0;
            var nbPlayersReady = 0;
            while(i < players.length && players[i].isReady ){
                i++;
            }
            if(i==players.length && players[i-1].isReady){
                socket.broadcast.to(salon).emit('lunchingGame');
                socket.emit('lunchingGame');
            }
        }
    });

    socket.on('start', function (data) {
        socket.emit('connected', {playerId: thisPlayer.playerid});
        console.log(getDT()+"Player ID : "+thisPlayer.playerid+", Username : "+thisPlayer.username+", team : "+thisPlayer.team+" connected");
        sync();
        socket.broadcast.to(salon).emit('update', {playerId: thisPlayer.playerid, x: 100, y: 100, sheet: "my_player", direction: "left"});
        if(!alreadyStarted){
            console.log(getDT()+"Chrono Started");
            alreadyStarted=true;
            startGame();
        }
    });

    socket.on('disconnect', function () {
        console.log(salon);
        if(salon != undefined) {
            salons[salon['id']]['nb']--;
            if (salons[salon['id']]['nb'] == 0) {
                delete salons[salon['id']];
            }
            salon = undefined;
            socket.leave(salons[salon].name);
        }
        socket.broadcast.emit('deco', {playerId: thisPlayer.playerid});
        console.log(getDT()+"Player ID : "+thisPlayer.playerid+" disconnected");
        socket.leave(salon);
        var pos = players.indexOf(thisPlayer);
        if (pos >= 0) {
            players.splice(pos, 1);
        }
        if(players.length == 0){
            console.log(getDT()+"Game Stopped ! Number of player is zero.");
            clearTimeout(intervalChrono);
            clearTimeout(intervalBoots);
            clearTimeout(intervalGun);
            clearTimeout(intervalBulleGun);
            pointR = 0;
            pointB = 0;
            alreadyStarted=false;
            timeSec = 0;
        }
        connection.end();
    });

    socket.on('update', function (data) {
        socket.broadcast.emit('updated', data);
    });
    
    socket.on('ping',function (data) {
        socket.emit('pong',{});
    });

    socket.on('chat', function (data) {
        socket.broadcast.to(salon).emit('chat', data);
    });

    socket.on('getObj', function (data) {
        socket.broadcast.to(salon).emit('getObj', data);
    });

    socket.on('newPoint', function (data) {
        if (data.team == 'R'){
            pointR += 1;  
        } else {
            pointB += 1;
        }
        sync();
    });

    function sync() {
        socket.broadcast.to(salon).emit('sync', {secondes: timeSec, pointR: pointR, pointB: pointB});
    }

    function startChrono() {
        intervalChrono=setInterval(chrono, 1000);
        intervalSync=setInterval(sync, 3000);
        intervalBoots=setInterval(boots, TIME_BOOTS);
        intervalBulleGun=setInterval(bulleGun, TIME_BULLEGUN);
        intervalGun=setInterval(Gun, TIME_GUN);
    }
    function chrono() {
        timeSec += 1;
        stopGame();
    }

    function boots() {
        socket.broadcast.to(salon).emit('getObj', {id : 1, obj: "Boots", function : "drop"});
        socket.broadcast.to(salon).emit('getObj', {id : 2, obj: "Boots", function : "drop"});
        socket.emit('getObj', {id : 1, obj: "Boots", function : "drop"});
        socket.emit('getObj', {id : 2, obj: "Boots", function : "drop"});
    }

    function bulleGun() {
        socket.broadcast.to(salon).emit('getObj', {id : 5, obj: "BulleGun", function : "drop"});
        socket.broadcast.to(salon).emit('getObj', {id : 4, obj: "BulleGun", function : "drop"});
        socket.emit('getObj', {id : 5, obj: "BulleGun", function : "drop"});
        socket.emit('getObj', {id : 4, obj: "BulleGun", function : "drop"});
    }

    function Gun() {
        socket.broadcast.to(salon).emit('getObj', {id : 3, obj: "Gun", function : "drop"});
        socket.emit('getObj', {id : 3, obj: "Gun", function : "drop"});
    }

    function startGame() {
        pointR = 0;
        pointB = 0;
        timeSec = 0;
        startChrono();
        socket.broadcast.to(salon).emit('startGame',{});
    }

    function stopGame() {
        if((timeSec == TIME_MAX) || (POINT_MAX == pointR) || (POINT_MAX == pointB)){
            console.log(getDT()+"Game Stopped ! Time: "+timeSec + "s PointR: " +pointR +" PointB :" +pointB);
            socket.broadcast.to(salon).emit('stopGame', {timeMax:TIME_MAX, secondes: timeSec, pointR: pointR, pointB: pointB});
            socket.emit('stopGame', {secondes: timeSec, pointR: pointR, pointB: pointB});
            clearTimeout(intervalChrono);
            clearTimeout(intervalBoots);
            clearTimeout(intervalGun);
            clearTimeout(intervalBulleGun);
            pointR = 0;
            pointB = 0;
            alreadyStarted=false;
            timeSec = 0;
        }
    }
});

function countTeam(teamName) {
    var numOfPlayer = 0;
    for(var i=0;i<players.length;i++){
        if(players[i].team == teamName)
           numOfPlayer++;
    }
    console.log(teamName+" possede "+numOfPlayer);
    return numOfPlayer;
}

function getDT(){
    var date = new Date();
    var str = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " - " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return "[ "+str+" ] ";
}



server.listen(88);
console.log(getDT()+"Multiplayer app listening on port 88");

