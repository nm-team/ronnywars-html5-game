Q.loadTMX(["startGame.tmx", 'lava.png', 'yeeah.mp3', /*'explosion.mp3','Level_Up.mp3','EDM_Detection_Mode.mp3',*/'raymond.png', 'backgroud.png', 'game-tiles.png', "game.tmx", /*'game.json','game-background.json',*/'gametiles_background.png', 'p1-sprite.png', 'p2-sprite.png', 'boots.png', 'flagbanim.png', 'flagranim.png', 'onepix.png', 'glock.png', "p1-sprite-flag.png", "p2-sprite-flag.png", "bullet.png", "bulleGun.png", "Bulles.png"], function () {

    console.log('Chargement des fichiers');

    Q.sheet('my_tiles', 'game-tiles.png', {tileW: 30, tileH: 30});
    Q.sheet('tiles_background', 'gametiles_background.png', {tileW: 30, tileH: 30});
    Q.sheet('flagB', 'flagbanim.png', {tileW: 30, tileH: 30});
    Q.sheet('flagR', 'flagranim.png', {tileW: 30, tileH: 30});
    Q.sheet('player_B', 'p1-sprite.png', {tileW: 28, tileH: 59});
    Q.sheet('player_B_flag', 'p1-sprite-flag.png', {tileW: 28, tileH: 59});
    Q.sheet('player_R', 'p2-sprite.png', {tileW: 28, tileH: 59});
    Q.sheet('player_R_flag', 'p2-sprite-flag.png', {tileW: 28, tileH: 59});
    Q.sheet('boots', 'boots.png', {tileW: 30, tileH: 24});
    Q.sheet('zone', 'onepix.png', {tileW: 30 * 4, tileH: 1});
    Q.sheet('lava', 'lava.png', {tileW: 30 * 92, tileH: 1});
    Q.sheet('gun', 'glock.png', {tileW: 28, tileH: 18});
    Q.sheet('bullet', 'bullet.png', {tileW: 8, tileH: 3});
    Q.sheet('bulleGun', 'bulleGun.png', {tileW: 25, tileH: 20});
    Q.sheet('bulle', 'Bulles.png', {tileW: 10, tileH: 10});

    console.log('Fichiers du niveau chargés');


    if (readCookie('session_id')) {
        console.log("session read");
        session_id = readCookie('session_id');
        socket.emit('checkSession', session_id);
        console.log("session check");
        socket.on('sessionOK', function (data) {
            my = data.playerInfo;
            username = my.username_user;
            Q.stageScene('salon', 0);
        });
        socket.on('sessionFailed', function () {
            Q.stageScene('startGame', 0);
        });
    } else {
        console.log("session create");
        Q.stageScene('startGame', 0);
        session_id = newsession_id;
        console.log(newsession_id);
        writeCookie('session_id', session_id, 3);
        socket.emit('openSession', session_id);
    }

}, {
    progressCallback: function (loaded, total) {
        console.log(Math.floor(loaded / total * 100) + "%");
        $('#divLoadBar').css("width", Math.floor(loaded / total * 100) + "%");
        $('#divLoadNum').text(Math.floor(loaded / total * 100) + "%");

    }
});
// 'explosion.mp3','Level_Up.mp3','EDM_Detection_Mode.mp3',