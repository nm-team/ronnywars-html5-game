Q.component("pistol", {
    added: function () {
        my.nbPistolGet_user++;
        this.entity.p.ammo = 10;
        $("#pistol").remove();
        $("#composent").append("<div id='pistol'>Pistol : 15</div>");
        console.log(this);
    },
    destroy: function () {
        this.entity.p.ammo = 0;
        $("#pistol").remove();
        delete(this.entity.pistol);
        this.destroy();
    },

    extend: {
        fire: function () {
            // We can use this.p to set properties
            // because fire is called directly on the player
            if (this.p.ammo > 0) {
                my.nbShot_user++;
                this.p.ammo -= 1;
                $("#pistol").text("Pistol : " + this.p.ammo);
                var p = this.p;
                //Q.audio.play('fire.mp3');
                if (Q.inputs['fire'] && p.direction == 'right') {
                    this.stage.insert(new Q.Bullet({
                        x: p.x + 30,
                        y: p.y + p.w / 2,
                        vx: player.p.vx + 600,
                        shooter: username,
                        shooterID: selfId
                    }));
                    socket.emit('getObj', {
                        shooterID: selfId,
                        obj: "Bullet",
                        x: p.x + 30,
                        y: p.y + p.w / 2,
                        vx: player.p.vx + 600,
                        username: username
                    });

                }
                else {
                    this.stage.insert(new Q.Bullet({
                        x: p.x - 30,
                        y: p.y + p.w / 2,
                        vx: player.p.vx - 600,
                        shooter: username,
                        shooterID: selfId
                    }));
                    socket.emit('getObj', {
                        shooterID: selfId,
                        obj: "Bullet",
                        x: p.x - 30,
                        y: p.y + p.w / 2,
                        vx: player.p.vx - 600,
                        username: username
                    });
                }


            } else {
                console.log("No ammo");
            }
        }
    }
});

Q.component("boots", {
    added: function () {
        my.nbBootsGet_user++;
        this.entity.p.speed = 600;
        this.entity.p.jumpSpeed = -900;
        $("#boots").remove();
        $("#composent").append("<div id='boots'>Boots</div>");
        console.log(this);
    },
    destroy: function () {
        this.entity.p.speed = 300;
        this.entity.p.jumpSpeed = -500;
        delete(this.entity.boots);
        $("#boots").remove();
        this.destroy();
    }
});

Q.component("bullePistol", {
    added: function () {
        my.nbPistolGet_user++;
        this.entity.p.bulle = 30;
        $("#pistol").remove();
        $("#composent").append("<div id='pistol'>BullePistol : 30</div>");
        console.log(this);
    },
    destroy: function () {
        this.entity.p.bulle = 0;
        $("#pistol").remove();
        delete(this.entity.bullePistol);
        this.destroy();
    },

    extend: {
        fire: function () {
            // We can use this.p to set properties
            // because fire is called directly on the player
            if (this.p.bulle > 0) {
                my.nbShot_user++;
                this.p.bulle -= 1;
                $("#pistol").text("BullePistol : " + this.p.bulle);
                var p = this.p;
                //Q.audio.play('fire.mp3');
                if (Q.inputs['fire'] && p.direction == 'right') {
                    this.stage.insert(new Q.Bulle({
                        x: p.x + 30,
                        y: p.y + p.w / 2,
                        vx: player.p.vx + 600,
                        shooter: username,
                        shooterID: selfId
                    }));
                    socket.emit('getObj', {
                        shooterID: selfId,
                        obj: "Bulle",
                        x: p.x + 30,
                        y: p.y + p.w / 2,
                        vx: player.p.vx + 600,
                        username: username
                    });
                }
                else {
                    this.stage.insert(new Q.Bulle({
                        x: p.x - 30,
                        y: p.y + p.w / 2,
                        vx: player.p.vx - 600,
                        shooter: username,
                        shooterID: selfId
                    }));
                    socket.emit('getObj', {
                        shooterID: selfId,
                        obj: "Bulle",
                        x: p.x - 30,
                        y: p.y + p.w / 2,
                        vx: player.p.vx - 600,
                        username: username
                    });
                }


            } else {
                console.log("No bulle");
            }
        }
    }
});