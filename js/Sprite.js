Q.Sprite.extend('Player', {
    init: function (p) {
        this._super(p, {
            sheet: 'player_' + team,
            sprite: 'player_' + team, // On indique le sprite
            type: SPRITE_PLAYER,
            collisionMask: SPRITE_ACTOR | Q.SPRITE_DEFAULT | SPRITE_OBJ | SPRITE_ZONE | SPRITE_BULLET  , // On verra ça au moment de la gestion des collisions
            sensor: true,
            speed: 300,
            jumpSpeed: -500, // Pour sauter plus haut, on diminue la valeur
            capture: false,
            score: 0,
            equipe: team,
            username: username,
            direction: null // Par défaut il n'y a pas de direction, notre joueur est statique
        });
        this.p.UI_text = new Q.UI.Text({
            x: this.p.x,
            y: this.p.y,
            color: "white",
            label: this.p.username,
            align: 'center',
            family: 'Comfortaa, cursive',
            size: 10 // C'est un titre, donc c'est gros
        });
        this.p.stage.insert(this.p.UI_text)
        Q.input.on("fire", this, "fireWeapon");
        this.add('2d, platformerControls, animation'); // Remplacer la ligne dans le *constructeur* pour ajouter le composant animation
        this.on('hit.sprite', function (collision) {
            if (collision.obj.isA('Lava')) {
                this.playerDie();
                socket.emit('chat', {username: username, message: username + " died in lava"});
                chat(username + " died in lava");
            }
        });
    },
    step: function (dt) {
        // On va déjà éviter que notre joueur ne sorte de la grille…

        if (this.p.x <= 10) {
            this.p.x = 10;
        }
        else if (this.p.x >= 2670) {
            this.p.x = 2670;
        }

        if (Q.inputs['up']) {
            this.play("jump", 1);      // add priority to animation
        } else if (this.p.vx > 0) {
            this.p.flip = "";          // flip when going right
            this.play("run_right");
        } else if (this.p.vx < 0) {
            this.p.flip = "x";           // no flip when going left
            this.play("run_left");
        } else {
            this.play("stand_" + this.p.direction); // stand_left or stand_right
        }
        this.p.socket.emit('update', {
            playerId: this.p.playerId,
            x: this.p.x,
            y: this.p.y,
            sheet: this.p.sheet,
            direction: this.p.direction,
            vx: this.p.vx,
            equipe: this.p.equipe,
            username: this.p.username
        });
        this.p.UI_text.p.x = this.p.x;
        this.p.UI_text.p.y = this.p.y - 40;

    },
    fireWeapon: function () {
        if (this.has("pistol")) {
            this.pistol.entity.fire();
            //Q.audio.play('fire.mp3');
        } else {
            if (this.has("bullePistol")) {
                this.bullePistol.entity.fire();
                //Q.audio.play('fire.mp3');
            }else{
               console.log("no gun");
            }
        }
    }
    /*,
    fire: function () {
        var p = this.p;
        Q.audio.play('fire.mp3');
        if (Q.inputs['fire'] && p.direction == 'right') {
            this.stage.insert(new Q.Bullet({
                x: p.x + 20,
                y: p.y + p.w / 2,
                vx: 300,
                username: username
            }));

        }
        else {
            this.stage.insert(new Q.Bullet({
                x: p.x - 20,
                y: p.y + p.w / 2,
                vx: -300,
                username: username
            }))
        }
    }*/,
    playerDie: function () {
        my.nbDeath_user++;
        if (this.p.equipe == "B") {
            this.p.x = Math.random() * ((spawn[this.p.equipe][0][0]) - (spawn[this.p.equipe][0][1])) + (spawn[this.p.equipe][0][1]);
            this.p.y = spawn[this.p.equipe][1][0];
            this.p.vx = 0;
            this.p.vy = 0;
        } else {
            this.p.x = Math.random() * ((spawn[this.p.equipe][0][0]) - (spawn[this.p.equipe][0][1])) + (spawn[this.p.equipe][0][1]);
            this.p.y = spawn[this.p.equipe][1][0];
            this.p.vx = 0;
            this.p.vy = 0;

        }
        if (this.p.capture) {
            this.p.sheet = 'player_' + team;
            $("#flag").text("No Flag");
            this.p.capture = false;
            if (this.p.equipe == "B") {
                dropFlag("flagR");
                flagR.active_true();
            } else {
                dropFlag("flagB");
                flagB.active_true();
            }
        }
        this.del("boots");
        this.del("pistol");
    }

});
var flagB;
Q.Sprite.extend('flagB', {
    init: function (p) {
        this._super(p, {
            sheet: 'flagB',
            sensor: true,
            collisionMask: Q.SPRITE_DEFAULT,
            type: SPRITE_OBJ,
            gravity: 0,
            opacity: 1,
            active: true
        });
        flagB = this;
        this.add('2d,animation');
        //this.play('stand');
        this.on('hit.sprite', function (collision) {
            if ((collision.obj.isA('Player') && collision.obj.p.equipe == "R") && this.p.active) {
                my.nbFlagGet_user++;
                console.log("CAPTURE FLAG!");
                socket.emit('chat', {username: username, message: username + " CAPTURE FLAG BLUE!"});
                chat(username + " CAPTURE FLAG BLUE!");
                this.active_false();

                socket.emit('getObj', {username: username, obj: "flagB", function: "capture"});
                collision.obj.p.sheet = 'player_' + team + "_flag"
                collision.obj.p.capture = true;
                $("#flag").text("CAPTURE FLAG");
            }
        });
    },
    active_true: function () {
        this.p.type = SPRITE_OBJ;
        this.p.opacity = 1;
        this.p.active = true;
        this.p.sensor = true;
    },
    active_false: function () {
        this.p.type = Q.SPRITE_NONE;
        this.p.opacity = 0;
        this.p.active = false;
        this.p.sensor = true;
    }
});
var flagR;
Q.Sprite.extend('flagR', {
    init: function (p) {
        this._super(p, {
            sheet: 'flagR',
            sensor: true,
            collisionMask: Q.SPRITE_DEFAULT,
            type: SPRITE_OBJ,
            gravity: 0,
            opacity: 1,
            active: true
        });
        flagR = this;
        this.add('2d,animation');
        //this.play('stand');
        this.on('hit.sprite', function (collision) {
            if ((collision.obj.isA('Player') && collision.obj.p.equipe == "B") && this.p.active) {
                my.nbFlagGet_user++;
                collision.obj.p.capture = true;
                collision.obj.p.sheet = 'player_' + team + "_flag";
                this.active_false();
                socket.emit('getObj', {username: username, obj: "flagR", function: "capture"});
                console.log("CAPTURE FLAG!");
                socket.emit('chat', {username: username, message: username + " CAPTURE FLAG RED!"});
                chat(username + " CAPTURE FLAG RED!");
                $("#flag").text("CAPTURE FLAG");
            }
        });
    },
    active_true: function () {
        this.p.type = SPRITE_OBJ;
        this.p.opacity = 1;
        this.p.active = true;
        this.p.sensor = true;
    },
    active_false: function () {
        this.p.type = Q.SPRITE_NONE;
        this.p.opacity = 0;
        this.p.active = false;
        this.p.sensor = true;
    }
});

Q.Sprite.extend('ZoneDropB', {
    init: function (p) {
        this._super(p, {
            sheet: 'my_tiles',
            frame: 3,
            collisionMask: SPRITE_PLAYER | Q.SPRITE_DEFAULT,
            sensor: true,
            type: SPRITE_ZONE
        });
        this.on('hit.sprite', function (collision) {
            if (collision.obj.isA('Player')) {
                if ((collision.obj.p.capture && collision.obj.p.equipe == "B") && !flagR.active) {
                    my.nbDropFlag_user++;
                    socket.emit('newPoint', {team: 'B'});
                    pointB += 1;
                    $("#scoreb").text("Score B : " + pointB);
                    console.log("DROP FLAG!");
                    socket.emit('chat', {username: username, message: username + " DROP FLAG BLUE!"});
                    dropFlag("flagR");
                    chat(username + " DROP FLAG!");
                    collision.obj.p.capture = false;
                    flagR.active_true();
                    $("#flag").text("No Flag");
                    collision.obj.p.sheet = 'player_' + team;
                }
            }
        });
    }

});

Q.Sprite.extend('ZoneDropR', {
    init: function (p) {
        this._super(p, {
            sheet: 'my_tiles',
            frame: 2,
            collisionMask: SPRITE_PLAYER | Q.SPRITE_DEFAULT | SPRITE_ZONE | SPRITE_OBJ,
            sensor: true,
            type: SPRITE_ZONE
        });
        this.on('hit.sprite', function (collision) {
            if (collision.obj.isA('Player')) {
                if ((collision.obj.p.capture && collision.obj.p.equipe == "R") && !flagB.active) {
                    my.nbDropFlag_user++;
                    socket.emit('newPoint', {team: 'R'});
                    pointR += 1;
                    $("#scorer").text("Score R : " + pointR);
                    console.log("DROP FLAG!");
                    socket.emit('chat', {username: username, message: username + " DROP FLAG RED!"});
                    dropFlag("flagB");
                    chat(username + " DROP FLAG RED!");
                    collision.obj.p.capture = false;
                    flagB.active_true();
                    $("#flag").text("No Flag");
                    collision.obj.p.sheet = 'player_' + team;
                }
            }
        });
    }

});

Q.Sprite.extend('Lava', {
    init: function (p) {
        this._super(p, {
            sheet: 'my_tiles',
            frame: 4,
            collisionMask: SPRITE_PLAYER,
            sensor: true,
            type: SPRITE_OBJ,
            vx: 0,
            vy: 0,
            gravity: 0
        });
    }
});


Q.Sprite.extend('Boots', {
    init: function (p) {
        this._super(p, {
            sheet: 'boots',
            collisionMask: SPRITE_PLAYER,
            sensor: true,
            type: SPRITE_OBJ,
            vx: 0,
            vy: 0,
            gravity: 0,
            active: true
        });
        this.on("sensor");
        this.on('hit', function (collision) {
            if (collision.obj.isA('Player') && this.p.active) {
                collision.obj.add("boots");
                socket.emit('getObj', {id: p.id_item, obj: "Boots", function: "capture"});
                //Q.audio.play('yeeah.mp3');
                setTimeout(function () {
                    collision.obj.del("boots");
                    console.log("Boots disable");
                }, 3000);

                this.active_false();
            }
        });
    },
    sensor: function (colObj) {
    },
    active_true: function () {
        this.p.type = SPRITE_OBJ;
        this.p.opacity = 1;
        this.p.active = true;
        this.p.sensor = true;
    },
    active_false: function () {
        this.p.type = Q.SPRITE_NONE;
        this.p.opacity = 0;
        this.p.active = false;
        this.p.sensor = true;
    }

});

Q.Sprite.extend('blockJump', {
    init: function (p) {
        this._super(p, {
            sheet: 'my_tiles',
            frame: 15,
            collisionMask: SPRITE_PLAYER,
            sensor: true,
            type: SPRITE_OBJ,
            vx: 0,
            vy: 0,
            gravity: 0
        });
        this.on("sensor");
        this.on('hit', function (collision) {
            if (collision.obj.p.vy - this.p.jSpeed >= -800) {
                collision.obj.p.vy = -800;
            } else {
                collision.obj.p.vy = collision.obj.p.vy - this.p.jSpeed;
            }
            collision.obj.p.y = collision.obj.p.y - 1;
        });
    },
    sensor: function (colObj) {
        colObj.p.vy = colObj.p.vy - this.p.jSpeed;
        colObj.p.y = colObj.p.y - 1;
    }


});

Q.Sprite.extend('Gun', {
    init: function (p) {
        this._super(p, {
            sheet: 'gun',
            collisionMask: Q.SPRITE_DEFAULT,
            sensor: true,
            type: SPRITE_OBJ
        });
        this.on('hit.sprite', function (collision) {
            if (collision.obj.isA('Player')) {
                socket.emit('getObj', {id: p.id_item, obj: "Gun", function: "capture"});
                if (collision.obj.pistol == undefined) {
                    collision.obj.add("pistol");
                } else {
                    collision.obj.p.ammo = 15;
                    $("#pistol").text("Pistol : " + collision.obj.p.ammo);
                }

                this.active_false();
            }
        });
    },
    active_true: function () {
        this.p.type = SPRITE_OBJ;
        this.p.opacity = 1;
        this.p.active = true;
        this.p.sensor = true;
    },
    active_false: function () {
        this.p.type = Q.SPRITE_NONE;
        this.p.opacity = 0;
        this.p.active = false;
        this.p.sensor = true;
    }

});

Q.Sprite.extend('BulleGun', {
    init: function (p) {
        this._super(p, {
            sheet: 'bulleGun',
            collisionMask: Q.SPRITE_DEFAULT,
            sensor: true,
            type: SPRITE_OBJ
        });
        this.on('hit.sprite', function (collision) {
            if (collision.obj.isA('Player')) {
                socket.emit('getObj', {id: p.id_item, obj: "BulleGun", function: "capture"});
                if (collision.obj.pistol == undefined) {
                    collision.obj.add("bullePistol");
                } else {
                    collision.obj.p.bulle = 30;
                    $("#pistol").text("BullePistol : " + collision.obj.p.bulle);
                }
                this.active_false();
            }
        });
    },
    active_true: function () {
        this.p.type = SPRITE_OBJ;
        this.p.opacity = 1;
        this.p.active = true;
        this.p.sensor = true;
    },
    active_false: function () {
        this.p.type = Q.SPRITE_NONE;
        this.p.opacity = 0;
        this.p.active = false;
        this.p.sensor = true;
    }

});

Q.Sprite.extend('Actor', {
    init: function (p) {
        this._super(p, {
            update: true,
            type: Q.SPRITE_DEFAULT,
            collisionMask: SPRITE_PLAYER, // On verra ça au moment de la gestion des collisions
            sensor: true,
            equipe: "",
            username: "",
            gravity: 0
        });
        this.p.UI_text = new Q.UI.Text({
            x: this.p.x,
            y: this.p.y,
            color: "white",
            label: this.p.username,
            align: 'center',
            family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
            size: 10 // C'est un titre, donc c'est gros
        });
        this.p.stage.insert(this.p.UI_text)
        /*
         var temp = this;
         setInterval(function () {
         if (!temp.p.update) {
         temp.destroy();
         }
         temp.p.update = false;
         }, 3000);*/
    }
});


Q.Sprite.extend("Bullet", {
    init: function (p) {
        this._super(p, {
            sheet: "bullet",
            sprite: "bullet",
            type: SPRITE_BULLET,
            collisionMask: SPRITE_ACTOR,
            sensor: true,
            gravity: 0,
            shooter: "",
            shooterID: 0
        });
        console.log(p);
        this.add("2d,animation");
        this.on('hit.sprite', function (collision) {
            var actor = players.filter(function (obj) {
                return obj.playerId == p.shooterID;
            })[0];
           // if (collision.obj.isA('Player') && (p.shooterID != selfId) && (team != actor.player.p.team)) {
            if (collision.obj.isA('Player')) {
                collision.obj.playerDie();
                socket.emit('chat', {
                    username: this.p.shooter,
                    message: this.p.shooter + " killed " + collision.obj.p.username
                });
                chat(this.p.shooter + " killed " + collision.obj.p.username);
                this.destroy();
            }

        });
        this.on('hit', function (collision) {
            this.destroy();
        });
    }
});

Q.Sprite.extend("Bulle", {
    init: function (p) {
        this._super(p, {
            sheet: "bulle",
            sprite: "bulle",
            type: SPRITE_BULLET,
            collisionMask: SPRITE_ACTOR,
            sensor: true,
            gravity: 0,
            shooter: "",
            shooterID: 0
        });
        console.log(p);
        this.add("2d,animation");
        this.on('hit.sprite', function (collision) {
            var actor = players.filter(function (obj) {
                return obj.playerId == p.shooterID;
            })[0];
            //if (collision.obj.isA('Player') && (p.shooterID != selfId) && (team != actor.player.p.team)) {
            if (collision.obj.isA('Player')) {
                console.log('if1');
                if(collision.obj.p.direction == 'right'){
                    collision.obj.p.vx = collision.obj.p.vx+30000;
                    collision.obj.p.x = collision.obj.p.x+1;
                    collision.obj.p.y = collision.obj.p.y-4;
                    console.log('if2a');
                }else{
                    collision.obj.p.vx = collision.obj.p.vx-30000;
                    collision.obj.p.x = collision.obj.p.x-1;
                    collision.obj.p.y = collision.obj.p.y-4;
                    console.log('if2b');
                }
                this.destroy();
            }
        });
        this.on('hit', function (collision) {
            this.destroy();
        });
    }
});