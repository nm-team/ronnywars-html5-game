Q.scene('team', function (stage) { // On crée une nouvelle scène que l'on nomme. Une fois affichée la fonction sera appelée, avec en paramètre notre objet scène (dont on récupèrera quelques infos et auquel on balancera quelques objets)
    $('#UI2').css('z-index', '-14');
    socket.emit('sendUsername',{username:username});
    var sprite_bg = new Q.Sprite({x: 0, y: 0, w: Q.width, h: Q.height, type: Q.SPRITE_UI});
    sprite_bg.draw = function (ctx) { // Au moment de dessiner le sprite, on récupère le contexte 2D du canvas pour dessiner librement
        ctx.fillStyle = '#e0b232'; // On veut du jaune
        ctx.fillRect(this.p.x, this.p.y, this.p.w, this.p.h); // On dessine un rectangle de la taille du sprite
    }
    stage.insert(sprite_bg);

    var title = stage.insert(new Q.UI.Text({
        x: Q.width / 2,
        y: 50,
        label: 'Choisir une equipe',
        align: 'center',
        family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
        size: 32, // C'est un titre, donc c'est gros
        color: '#aa4242' // Un rouge foncé, comme un bon verre de rouge… (hips !)
    })); // On insère un titre sous forme de texte en haut, centré

    var container = stage.insert(new Q.UI.Container({ // On crée un conteneur
        x: Q.width / 2, // On le centre en largeur
        y: Q.height / 2, // On le centre en hauter
        fill: 'rgba(0, 0, 0, 0.5)', // On applique un fond noir semi-transparent
        radius: 5 // Des bordures arrondies de 5 pixels pour faire joli
    }));
    socket.on('defineTeam',function(data){
        nbTeamB = data['teamB'];
        nbTeamR = data['teamR'];
        socket.off('defineTeam');

        var buttonBlueTeam = container.insert(new Q.UI.Button({
            x: 0,
            y: -30,
            fill: '#333',
            label: 'Equipe Bleu\n\rJoueurs : '+nbTeamB,
            align: 'center',
            highlight: '#ffffff',
            radius: 2
        })); // On insère un bouton dans notre conteneur, avec un fond blanc cassé, qui devient blanc au clic, en haut du conteneur
        var buttonRedTeam = container.insert(new Q.UI.Button({
            x: 0,
            y: 30,
            fill: '#333',
            label: 'Equipe Rouge\n\rJoueurs : '+nbTeamR,
            align: 'center',
            highlight: '#ffffff',
            radius: 2
        })); // On insère un bouton dans notre conteneur, avec un fond blanc cassé, qui devient blanc au clic, en haut du conteneur

        if(nbTeamB < nbTeamR ||  (nbTeamB == nbTeamR)) {
            buttonBlueTeam.p.fill = "#f7f7f7";
            buttonBlueTeam.on('click', function () { // On place un écouteur sur le bouton pour gérer le clic
                team = "B";
                Q.clearStages(); // On vide les scènes affichées, pour repartir sur un canvas vierge
                console.log('Bouton Bleu cliqué, lancement du jeu…'); // Regardez votre console ;)
                Q.audio.stop();
                socket.emit("sendTeam", {team: team});
                Q.stageScene('ready', 0); // On affiche une autre scène (qui sera crée dans la partie 3) au rang 0, soit tout en bas dans la pile des calques
            });
        }
        if((nbTeamB > nbTeamR) ||  (nbTeamB == nbTeamR )){
            buttonRedTeam.p.fill = "#f7f7f7";
            buttonRedTeam.on('click', function () { // On place un écouteur sur le bouton pour gérer le clic
                team = "R";
                Q.clearStages(); // On vide les scènes affichées, pour repartir sur un canvas vierge
                console.log('Bouton Rouge cliqué, lancement du jeu…'); // Regardez votre console ;)
                Q.audio.stop();
                socket.emit("sendTeam", {team: team});
                Q.stageScene('ready', 0); // On affiche une autre scène (qui sera crée dans la partie 3) au rang 0, soit tout en bas dans la pile des calques
            });
        }

        container.fit(10);
        console.log('Écran de lancement affiché');
    });



     // On adapte la taille du conteneur à son contenu (le bouton), avec un padding (marge interne) de 10 pixels
    //Q.audio.play('Level_Up.mp3',{ loop: true });
});
