Q.scene('StopGame', function (stage) { // On crée une nouvelle scène que l'on nomme. Une fois affichée la fonction sera appelée, avec en paramètre notre objet scène (dont on récupèrera quelques infos et auquel on balancera quelques objets)
    $("#username").css("display", "none");
    $("#time").css("display", "none");
    $("#scoreb").css("display", "none");
    $("#scorer").css("display", "none");
    $("#player").css("display", "none");

    $("#ping").css("display", "none");

    var sprite_bg = new Q.Sprite({x: 0, y: 0, w: Q.width, h: Q.height, type: Q.SPRITE_UI});
    sprite_bg.draw = function (ctx) { // Au moment de dessiner le sprite, on récupère le contexte 2D du canvas pour dessiner librement
        ctx.fillStyle = '#e0b232'; // On veut du jaune
        ctx.fillRect(this.p.x, this.p.y, this.p.w, this.p.h); // On dessine un rectangle de la taille du sprite
    }
    stage.insert(sprite_bg);


    var title = stage.insert(new Q.UI.Text({
        x: Q.width / 2,
        y: 50,
        label: 'Fin du Game',
        align: 'center',
        family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
        size: 32, // C'est un titre, donc c'est gros
        color: '#aa4242' // Un rouge foncé, comme un bon verre de rouge… (hips !)
    })); // On insère un titre sous forme de texte en haut, centré

    var container = stage.insert(new Q.UI.Container({ // On crée un conteneur
        x: Q.width / 2, // On le centre en largeur
        y: (Q.height / 2) - 50, // On le centre en hauter
        fill: 'rgba(0, 0, 0, 0.5)', // On applique un fond noir semi-transparent
        radius: 5 // Des bordures arrondies de 5 pixels pour faire joli
    }));


    var TimeUI = container.insert(new Q.UI.Text({
        x: 0,
        y: -90,
        fill: '#f7f7f7',
        label: 'Time: ' + minutes + ":" + secondes,
        align: 'center',
        family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
        size: 32, // C'est un titre, donc c'est gros
        color: '#000000' // Un rouge foncé, comme un bon verre de rouge… (hips !)
    }));

    var scoreBUI = container.insert(new Q.UI.Text({
        x: 0,
        y: -60,
        fill: '#f7f7f7',
        label: 'Score B : ' + pointB,
        align: 'center',
        family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
        size: 32, // C'est un titre, donc c'est gros
        color: '#000000' // Un rouge foncé, comme un bon verre de rouge… (hips !)
    }));

    var scoreRUI = container.insert(new Q.UI.Text({
        x: 0,
        y: -30,
        fill: '#f7f7f7',
        label: 'Score R : ' + pointR,
        align: 'center',
        family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
        size: 32, // C'est un titre, donc c'est gros
        color: '#000000' // Un rouge foncé, comme un bon verre de rouge… (hips !)
    }));

    var buttonReturn = container.insert(new Q.UI.Button({
        x: 0,
        y: 20,
        fill: '#f7f7f7',
        label: 'Menu Principal',
        align: 'center',
        highlight: '#ffffff',
        radius: 2
    }));

    if (team == "B" && pointB > pointR) {
        my.nbWinGame_user++;
    } else {
        if (team == "R" && pointR > pointB) {
            my.nbWinGame_user++;
        }
    }
    my.nbPlayedGame_user++;
    my.nbDeath_user--; //correction du décalage

    socket.emit('saveStats', my);

    buttonReturn.on('click', function () { // On place un écouteur sur le bouton pour gérer le clic
        Q.clearStages(); // On vide les scènes affichées, pour repartir sur un canvas vierge
        Q.audio.stop();
        Q.stageScene('startGame', 0); // On affiche une autre scène (qui sera crée dans la partie 3) au rang 0, soit tout en bas dans la pile des calques
        $("#sendChat").css("display", "none");
        $("#chat").css("display", "none");
        capture_flag = false;
        pointB = 0;
        pointR = 0;
        players = null;
        players = [];
        pingDate = 0;
        team = null;
        selfId = 0;
        secondes = 0;
        minutes = 0;
        socket.off('isLogged');
        socket.off('isRegister');
        socket.off('defineTeam');
        socket.off('readyPlayers');
        socket.off('lunchingGame');
        socket.off('connected');
        socket.off('update');
        socket.off('deco');
        socket.off('updated');
        socket.off('pong');
        socket.off('chat');
        socket.off('getObj');
        socket.off('sync');
        socket.off('startGame');
        socket.off('stopGame');
    });


    container.fit(10); // On adapte la taille du conteneur à son contenu (le bouton), avec un padding (marge interne) de 10 pixels
    //Q.audio.play('Level_Up.mp3',{ loop: true });
});