Q.scene('ready', function (stage) {

    $("#table").css("display", "block");
    var sprite_bg = new Q.Sprite({x: 0, y: 0, w: Q.width, h: Q.height, type: Q.SPRITE_UI});
    sprite_bg.draw = function (ctx) { // Au moment de dessiner le sprite, on récupère le contexte 2D du canvas pour dessiner librement
        ctx.fillStyle = '#e0b232'; // On veut du jaune
        ctx.fillRect(this.p.x, this.p.y, this.p.w, this.p.h); // On dessine un rectangle de la taille du sprite
    }
    stage.insert(sprite_bg);
    var readyStatus = false;
    var title = stage.insert(new Q.UI.Text({
        x: Q.width / 2,
        y: 50,
        label: 'Ready or Not Ready',
        align: 'center',
        family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
        size: 32, // C'est un titre, donc c'est gros
        color: '#aa4242' // Un rouge foncé, comme un bon verre de rouge… (hips !)
    })); // On insère un titre sous forme de texte en haut, centré

    var container = stage.insert(new Q.UI.Container({ // On crée un conteneur
        x: Q.width / 2, // On le centre en largeur
        y: Q.height / 2, // On le centre en hauter
        fill: 'rgba(0, 0, 0, 0.5)', // On applique un fond noir semi-transparent
        radius: 5 // Des bordures arrondies de 5 pixels pour faire joli
    }));

    var ready = container.insert(new Q.UI.Button({
        x: 0,
        y: 30,
        fill: 'green',
        label: 'Ready ?',
        align: 'center',
        highlight: '#ffffff',
        radius: 2
    })); // On insère un bouton dans notre conteneur, avec un fond blanc cassé, qui devient blanc au clic, en haut du conteneur

    ready.on('click', function () { // On place un écouteur sur le bouton pour gérer le clic
        if(!readyStatus) {
            ready.p.fill = 'red';
            ready.p.label = 'Not ready !';
            readyStatus = true;
            socket.emit("sendReady", {ready: readyStatus});
        }else{
            ready.p.fill = 'green';
            ready.p.label = 'Ready ?';
            readyStatus = false;
            socket.emit("sendReady", {ready: readyStatus});
        }
    });
    container.fit(10);
    socket.on('readyPlayers',function(data) {
        var players = data.players
            $(".j").remove();
        for (var i in data.players){
            var player = data.players[i];
            if(player.team == "B"){
                var R = "Not Ready";
                if(player.isReady){ R = "Ready"; }
                $("#jB").append("<div class='j'>"+player.username+" "+R+"</div>")
            }else{
                var R = "Not Ready";
                if(player.isReady){ R = "Ready"; }
                $("#jR").append("<div class='j'>"+player.username+" "+R+"</div>")
            }
        }
    });

    socket.emit("sendReady", {ready: false});

    socket.on('lunchingGame',function(){
        ready.off('click');
        ready.p.fill = 'green';
        ready.p.label = 'Ready ?';
        var compteur = stage.insert(new Q.UI.Text({
            x: Q.width / 2,
            y: 600,
            label: 'Ready !',
            align: 'center',
            family: 'Comfortaa, cursive',
            size: 32,
            color: '#2BA802'
        }));
        var a =4;
        var $compte =setInterval(function(){
            a--;
            if(a == 0){
                a = "Go!";
            }
            compteur.p.label = a+"";
            console.log(
                compteur.p);
            if(a == "Go!"){
                $("#table").css("display", "none");
                $(".j").remove();
                clearInterval($compte);
                Q.clearStages();
                socket.off('lunchingGame');
                socket.off('readyPlayers');
                Q.stageScene('game', 0);
            }
        },1000);
    });
});