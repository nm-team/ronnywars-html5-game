Q.scene('register', function (stage) { // On crée une nouvelle scène que l'on nomme. Une fois affichée la fonction sera appelée, avec en paramètre notre objet scène (dont on récupèrera quelques infos et auquel on balancera quelques objets)

    stage.insert(new Q.Repeater({
        asset: 'backgroud.png',
        speedX: 0.5,
        speedY: 0.5
    }));

    var title = stage.insert(new Q.UI.Text({
        x: Q.width / 2,
        y: 200,
        label: 'Register',
        font: "400 48px Comfortaa",
        align: 'center',
        family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
        size: 48, // C'est un titre, donc c'est gros
        color: '#aa4242' // Un rouge foncé, comme un bon verre de rouge… (hips !)
    })); // On insère un titre sous forme de texte en haut, centré


    var img_bg = new Q.Sprite({
        x: Q.width / 2,
        y: (Q.height / 2),
        w: Q.width,
        h: Q.height,
        tileW: Q.width,
        tileH: Q.width,
        asset: 'raymond.png'
    }); // On ajoute notre image en spécifiant l'asset à utiliser, les dimensions à lui donner et la partie de l'image à utiliser (ici 600x800, soit la taille du canvas)
    img_bg.add('tween');

    stage.insert(img_bg); // Ne pas oublier d'insérer l'image (à noter que vous pouvez tout faire en une seule ligne, comme déjà vu plus tôt)
    moveSheep.apply(img_bg);

    var container = stage.insert(new Q.UI.Container({ // On crée un conteneur
        x: (Q.width / 2) - 80, // On le centre en largeur
        y: (Q.height / 2), // On le centre en hauter
        fill: 'rgba(0, 0, 0, 0.5)', // On applique un fond noir semi-transparent
        radius: 5 // Des bordures arrondies de 5 pixels pour faire joli
    }));

    var container2 = stage.insert(new Q.UI.Container({ // On crée un conteneur
        x: (Q.width / 2) + 88, // On le centre en largeur
        y: (Q.height / 2), // On le centre en hauter
        fill: 'rgba(0, 0, 0, 0.5)', // On applique un fond noir semi-transparent
        radius: 5 // Des bordures arrondies de 5 pixels pour faire joli
    }));
    var buttonBack = container.insert(new Q.UI.Button({
        x: 0,
        y: 0,
        fill: '#f7f7f7',
        label: 'BACK',
        align: 'center',
        highlight: '#ffffff',
        radius: 2,
        font: "400 18px Comfortaa"
    }));// On insère un bouton dans notre conteneur, avec un fond blanc cassé, qui devient blanc au clic, en haut du conteneur
    var buttonRegister = container2.insert(new Q.UI.Button({
        x: 0,
        y: 0,
        fill: '#f7f7f7',
        label: 'OK',
        align: 'center',
        highlight: '#ffffff',
        radius: 2,
        font: "400 18px Comfortaa"
    }));
    console.log('Écran de lancement affiché');
    buttonBack.on('click', function () { // On place un écouteur sur le bouton pour gérer le clic
        $('#failed').remove();
        Q.clearStages(); // On vide les scènes affichées, pour repartir sur un canvas vierge
        $('.divInput').remove();
        Q.stageScene('startGame', 0); // On affiche une autre scène (qui sera crée dans la partie 3) au rang 0, soit tout en bas dans la pile des calques

    });

    buttonRegister.on('click', function () { // On place un écouteur sur le bouton pour gérer le clic
        $('#failed').remove();
        if (($('#password1').val() != undefined) && ($('#password1').val() == $('#password2').val()) && ($('#email').val() != undefined) && ($('#username1').val() != undefined)) {
            socket.emit("register", {
                username: $('#username1').val(),
                password: $('#password1').val(),
                email: $("#email").val()
            });
        } else {
            $('#game_container').append("<span id='failed'>Invalid Data</span>");
        }
        socket.on("isRegister", function (data) {
            if (data["isRegister"]) {
                Q.clearStages(); // On vide les scènes affichées, pour repartir sur un canvas vierge
                $('.divInput').remove();

                Q.stageScene('startGame', 0); // On affiche une autre scène (qui sera crée dans la partie 3) au rang 0, soit tout en bas dans la pile des calques
            } else {
                $('#game_container').append("<span id='failed'>Register Failed !</span>");
            }
        });
    });


    container.fit(10); // On adapte la taille du conteneur à son contenu (le bouton), avec un padding (marge interne) de 10 pixels
    container2.fit(10); // On adapte la taille du conteneur à son contenu (le bouton), avec un padding (marge interne) de 10 pixels
    $('#game_container').append("<div class='divInput'><input id='username1' type='text' name='username1' placeholder='Username' required=''></div>");
    $('#game_container').append("<div class='divInput'><input id='password1' type='password' name='password1' placeholder='Password' required=''></div>");
    $('#game_container').append("<div class='divInput'><input id='password2' type='password' name='password2' placeholder='Password Confirmation' required=''></div>");
    $('#game_container').append("<div class='divInput'><input id='email' type='email' name='email' placeholder='Email@domain.com' required=''></div>");
    $('.divInput').css('left', ((Q.width / 2) - (235 / 2) - 10) + 'px');

    $('#sendMess').keypress(function (e) {
        if (e.which == 13 && $('#sendMess').val() != "") {
            socket.emit('', {username: username, message: username + " : " + $('#sendMess').val()});
            chat(username + " : " + $('#sendMess').val());
            $('#sendMess').val("");
        }
    });

    //Q.audio.play('Level_Up.mp3',{ loop: true });
});
