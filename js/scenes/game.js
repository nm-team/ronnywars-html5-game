Q.scene('game', function (stage) {

    console.log('Niveau 1 !');

    stage.insert(new Q.Repeater({
        asset: 'backgroud.png',
        speedX: 0.5,
        speedY: 0.5
    }));

    socket.on('chat',function(data){
        chat(data['message']);
    });


    Q.stageTMX("game.tmx",stage);

    socket.on('updated', function (data) {
        var actor = players.filter(function (obj) {
            return obj.playerId == data['playerId'];
        })[0];
        if (actor) {
            actor.player.p.x = data['x'];
            actor.player.p.y = data['y'];

            actor.player.p.UI_text.p.x = data['x'];
            actor.player.p.UI_text.p.y = data['y'] - 40;
            actor.player.p.sheet = data['sheet'];
            actor.player.p.update = true;
            actor.player.p.direction = data['direction'];
            actor.player.p.vx = data['vx'];
            actor.player.p.equipe = data['equipe'];
            actor.player.p.stage = stage;
            actor.player.p.username = data['username'];
            if (actor.player.p.vx > 0) {
                actor.player.p.flip = "";          // flip when going right
            } else if (actor.player.p.vx < 0) {
                actor.player.p.flip = "x";           // no flip when going left
            }
        } else {
            var temp = new Q.Actor({
                playerId: data['playerId'],
                x: data['x'],
                y: data['y'],
                sheet: data['sheet'],
                direction: data['direction'],
                equipe: data['equipe'],
                stage: stage,
                username: data['username']
            });
            players.push({player: temp, playerId: data['playerId']});
            stage.insert(temp);
        }
    });

    $("#time").css("display", "block");
    $("#scoreb").css("display", "block");
    $("#scorer").css("display", "block");
    $("#player").css("display", "block");
    $("#sendChat").css("display", "block");
    $("#chat").css("display", "block");

    $("#ping").css("display", "block");

    $("#time").text("00:00");
    $("#scoreb").text("Score B : 0");
    $("#scorer").text("Score R : 0");


    function chrono() {
        secondes += 1;

        if (secondes > 59) {
            minutes += 1;
            secondes = 0;
        }

        if (minutes < 10 && secondes < 10) {
            $("#time").text("0" + minutes + " : 0" + secondes);
        }
        else if (minutes < 10 && secondes >= 10) {
            $("#time").text("0" + minutes + " : " + secondes);
        }
        else if (minutes >= 10 && secondes < 10) {
            $("#time").text(+minutes + " : 0" + secondes);
        }
        else if (minutes >= 10 && secondes > 10) {
            $("#time").text(+minutes + " : " + secondes);
        }
    }

    var timeChrono = setInterval(chrono, 1000);


    function ping() {
        socket.emit('ping', {});
        datePing = Date.now();
    }

    setInterval(ping, 1000);

    socket.on('pong', function (data) {
        latency = Date.now() - datePing;
        $("#ping").text("Ping : " + latency + " ms");
    });

    socket.on('sync', function (data) {
        minutes = Math.floor((data.secondes % 3600) / 60);
        secondes = Math.floor(((data.secondes % 3600) % 60));
        pointB = 0;
        pointR = 0;
        updateScoreTime('R', data.pointR);
        updateScoreTime('B', data.pointB);
    });


    w = stage.options.w;
    h = stage.options.h;


    socket.on('getObj', function (data) { // {id : id, obj: "Gun", function : "capture", id: 1}

        switch (data['obj']) {
            case 'flagR' :
                if (data['function'] == "capture") {
                    stage.lists.flagR[0].active_false();
                } else {
                    stage.lists.flagR[0].active_true();
                }
                break;
            case 'flagB' :
                if (data['function'] == "capture") {
                    stage.lists.flagB[0].active_false();
                } else {
                    stage.lists.flagB[0].active_true();
                }
                break;
            case 'Gun' :
                var index = stage.lists.Gun.filter(function (obj) {
                    return obj.p.id_item == data['id'];
                })[0];
                if (data['function'] == "capture") {
                    index.active_false();
                } else {
                    index.active_true();
                }
                break;
            case 'BulleGun' :
                var index = stage.lists.BulleGun.filter(function (obj) {
                    return obj.p.id_item == data['id'];
                })[0];
                if (data['function'] == "capture") {
                    index.active_false();
                } else {
                    index.active_true();
                }
                break;
            case 'Boots' :
                var index = stage.lists.Boots.filter(function (obj) {
                    return obj.p.id_item == data['id'];
                })[0];
                if (data['function'] == "capture") {
                    index.active_false();
                } else {
                    index.active_true();
                }
                break;
            case 'Bullet' :
                var bullet = new Q.Bullet({
                    x: data.x,
                    y: data.y,
                    vx: data.vx,
                    shooterID: data.shooterID,
                    shooter: data.username
                });
                Q.stage().insert(bullet);
                break;
            case 'Bulle' :
                var bulle = new Q.Bulle({
                    x: data.x,
                    y: data.y,
                    vx: data.vx,
                    shooterID: data.shooterID,
                    shooter: data.username
                });
                Q.stage().insert(bulle);
                break;
        }
    });

    socket.on('stopGame', function (data) { // secondes
        clearInterval(timeChrono);
        minutes = Math.floor((data.secondes % 3600) / 60);
        secondes = Math.floor(((data.secondes % 3600) % 60));
        pointB = 0;
        pointR = 0;
        updateScoreTime('R', data.pointR);
        updateScoreTime('B', data.pointB);
        Q.clearStages(); // On vide les scènes affichées, pour repartir sur un canvas vierge
        Q.audio.stop();
        Q.stageScene('StopGame', 0); // On affiche une autre scène (qui sera crée dans la partie 3) au rang 0, soit tout en bas dans la pile des calques
    });

    socket.on('connected', function (data) {
        selfId = data['playerId'];
        player = new Q.Player({playerId: selfId, x: 0, y: 0, equipe: team, socket: socket, stage: stage});//Math.random() * ((spawn[team][1]) - (spawn[team][0])) + (spawn[team][0])
        stage.insert(player);
        player.playerDie();
        stage.add('viewport').follow(player, {x: true, y: true}, {minX: 0, maxX: 2700, minY: 0, maxY: 1100});

        stage.viewport.offsetX = 90;
        stage.viewport.scale = 1;
    });

    function updateScoreTime(team, score) {
        if (team == 'R') {
            pointR = score;
            $("#scorer").text("Score R : " + pointR);
        } else {
            pointB = score;
            $("#scoreb").text("Score B : " + pointB);
        }
    }

    //Q.audio.play('EDM_Detection_Mode.mp3',{ loop: true });

    socket.emit('start');
});
