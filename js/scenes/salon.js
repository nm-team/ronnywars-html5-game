Q.scene('salon', function (stage) { // On crée une nouvelle scène que l'on nomme. Une fois affichée la fonction sera appelée, avec en paramètre notre objet scène (dont on récupèrera quelques infos et auquel on balancera quelques objets)

    stage.insert(new Q.Repeater({
        asset: 'backgroud.png',
        speedX: 0.5,
        speedY: 0.5
    }));

    $('#UI2').css('z-index', '2');

    var title = stage.insert(new Q.UI.Text({
        x: Q.width / 2,
        y: 200,
        label: 'Salon',
        font: "400 48px Comfortaa",
        align: 'center',
        family: 'Comfortaa, cursive', // Oui, du Comic Sans ! Pourquoi pas ?
        size: 48, // C'est un titre, donc c'est gros
        color: '#aa4242' // Un rouge foncé, comme un bon verre de rouge… (hips !)
    })); // On insère un titre sous forme de texte en haut, centré


    var container = stage.insert(new Q.UI.Container({ // On crée un conteneur
        x: (Q.width / 2) - 80, // On le centre en largeur
        y: (Q.height / 2), // On le centre en hauter
        fill: 'rgba(0, 0, 0, 0.5)', // On applique un fond noir semi-transparent
        radius: 5 // Des bordures arrondies de 5 pixels pour faire joli
    }));

    var container2 = stage.insert(new Q.UI.Container({ // On crée un conteneur
        x: (Q.width / 2) + 88, // On le centre en largeur
        y: (Q.height / 2), // On le centre en hauter
        fill: 'rgba(0, 0, 0, 0.5)', // On applique un fond noir semi-transparent
        radius: 5 // Des bordures arrondies de 5 pixels pour faire joli
    }));
    $('#UI2').append('<div id="tab"><div id="button"></div><div id="list"><div class="salon" data="idsalon"><div class="name_salon">Name</div><div class="lock">&nbsp;</div><div class="nb_salon"><div class="teamB">Team B</div><div class="teamR">Team R</div></div></div></div></div>');
    $("#button").append('<button id="new"><i class="fa fa-plus"></i> New</button>');
    $("#button").append('<button id="refresh"><i class="fa fa-refresh"></i> Refresh</button>');
    $("#button").append('<button id="search" class="right"><i class="fa fa-search"></i> Find Player</button>');

    $('#new').on("click", function () {
        //$("#list").append('<div class="salon" data="idsalon"><div class="name_salon">Nom_SALON</div><div class="lock"><i class="fa fa-lock"></i></div><div class="nb_salon"><div class="teamB">1</div><div class="teamR">2</div></div></div>');
        $('#game_container').append('<div id="createSalon">' +
        '<input type="text" name="Name Salon" id="nameSalon"/>' +
        '<input type="checkbox" id="checkLock" name="Lock Salon"> Private' +
        '<div id="pass"></div>' +
        '<button id="submitSalon">Create</button> <button id="cancelSalon">Cancel</button>' +
        '</div>');

        $('#checkLock').on('click', function () {
            if ($('#checkLock').is(':checked')) {
                $('#pass').append('<input type="text" id="passwordSalon" name="Password Salon"/>')
            } else {
                $('#passwordSalon').remove();
            }
        });

        $('#submitSalon').on('click', function () {
            socket.emit('createSalon', {
                name: $('#nameSalon').val(),
                pass: {lock: $('#checkLock').prop('checked'), pass: $('#nameSalon').val()}
            });
            $('#createSalon').remove();
            clearInterval($refresh);
            $('#tab').remove();
            Q.clearStages(); // On vide les scènes affichées, pour repartir sur un canvas vierge
            Q.stageScene('team', 0); // On affiche une autre scène (qui sera crée dans la partie 3) au rang 0, soit tout en bas dans la pile des calques
        });
        $('#cancelSalon').on('click', function () {
            $('#createSalon').remove();
        });

    });

    $('#refresh').on('click', function () {
        refresh();
    });

    function refresh() {
        $(".salon").remove();
        $("#list").append('<div class="salon" data="idsalon"><div class="name_salon">Name</div><div class="lock">&nbsp;</div><div class="nb_salon"><div class="teamB">Team B</div><div class="teamR">Team R</div></div>');
        socket.emit('refresh');
        socket.on('refresh', function (data) {
            socket.off('refresh');
            console.log(data.data);
            for (var n in data) {
                item = data[n];
                $("#list").append('<div class="salon" data="' + item.name + '"><div class="name_salon">' + item.name + '</div><div class="lock"></div><div class="nb_salon"><div class="teamB">0</div><div class="teamR">0</div></div></div>');
                if (item.pass.lock) {
                    $('div[data=' + item.name + '] .lock').append('<i class="fa fa-lock"></i>');
                } else {
                    $('div[data=' + item.name + '] .lock').append('&nbsp;');
                }
                $('div[data=' + item.name + ']').on('click', function () {
                    socket.emit('joinSalon', item.name);
                    console.log("joinSalon " + item.name);
                    socket.on("joinSuccess", function () {
                        socket.off("joinSuccess");
                        clearInterval($refresh);
                        $('#tab').remove();
                        Q.clearStages(); // On vide les scènes affichées, pour repartir sur un canvas vierge
                        Q.stageScene('team', 0); // On affiche une autre scène (qui sera crée dans la partie 3) au rang 0, soit tout en bas dans la pile des calques
                    });
                });
            }
        });
    }

    $refresh = setInterval(refresh(), 1000);

    container.fit(10);
    container2.fit(10); // On adapte la taille du conteneur à son contenu (le bouton), avec un padding (marge interne) de 10 pixels

    //Q.audio.play('Level_Up.mp3',{ loop: true });
});
