
Q.animations('player_B', {
    run_left: {frames: [0, 1, 2, 3, 4, 5], next: 'stand_left', rate: 1 / 6},
    run_right: {frames: [5, 4, 3, 2, 1, 0], next: 'stand_right', rate: 1 / 6},
    stand_left: {frames: [0]},
    stand_right: {frames: [0]},
    jump: {frames: [2], loop: false, rate: 1}
});

Q.animations('player_R', {
    run_left: {frames: [0, 1, 2], next: 'stand_left', rate: 1 / 6},
    run_right: {frames: [2, 1, 0], next: 'stand_right', rate: 1 / 6},
    stand_left: {frames: [0]},
    stand_right: {frames: [0]},
    jump: {frames: [2], loop: false, rate: 1}
});


/*
 Q.animations('flagB', {
 stand: { frames: [0,1,2,3], next: 'stand', rate: 1/6},
 });

 Q.animations('flagR', {
 stand: { frames: [0,1,2,3], next: 'stand', rate: 1/6},
 });*/